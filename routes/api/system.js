const express = require("express");
const passport = require("passport");

const router = express.Router();

const db = require("../../config/firebase");
const isEmpty = require("../../util/isEmpty");

// @route   GET api/system/status
// @desc    Get current status server
// @access  Public
router.get("/status", (req, res) => {
  const configRef = db.collection("system").doc("config");

  configRef
    .get()
    .then(doc => {
      res.json({ status: doc.data().status });
    })
    .catch(err => {
      res.statusCode = 500;
      res.json({ message: "Uppss.. Something wrong " });
    });
});

// @route   GET api/system/version
// @desc    Get latest version
// @access  Public
router.get("/version", (req, res) => {
  const configRef = db.collection("system").doc("config");

  configRef
    .get()
    .then(doc => {
      doc
        .data()
        .current_version.get()
        .then(doc2 => {
          res.json(doc2.data());
        })
        .catch(err => {
          res.statusCode = 500;
          res.json({ message: "Uppss.. Something wrong " });
        });
    })
    .catch(err => {
      res.statusCode = 500;
      res.json({ message: "Uppss.. Something wrong " });
    });
});

// @route   POST api/system/dependencies
// @desc    Get some parsing dependencies
// @access  Private
router.post(
  "/dependencies",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const version = req.query.version;
    const file = req.query.file;

    if (isEmpty(file) || isEmpty(version)) {
      res.statusCode = 400;
      return res.json({ message: "Version and file dependencies is required" });
    }

    //res.setHeader("Accept-Encoding", "gzip");
    //res.json({ adf: "asd" });
    res.download(`public/${version}/${file}`);
  }
);

module.exports = router;
