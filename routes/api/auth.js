const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const passport = require("passport");

const router = express.Router();

const db = require("../../config/firebase");
const isEmpty = require("../../util/isEmpty");
const tokenUtil = require("../../util/token");

// @route   POST api/auth/login
// @desc    Login user
// @access  Public
router.post("/login", (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  if (isEmpty(username) || isEmpty(password)) {
    res.statusCode = 400;
    return res.json({ message: "Username and password field is required" });
  }

  const userRef = db.collection("users");
  userRef
    .where("username", "==", username)
    .get()
    .then(snaps => {
      let data = [];

      snaps.forEach(doc => {
        data.push(doc.data());
      });

      // Username not found
      if (data.length < 1) {
        res.statusCode = 400;
        return res.json({ message: "Username not found" });
      }

      // Set to constant
      const user = data[0];

      // Checking the bcrypt encryption
      bcrypt.compare(password, user.password).then(isMatch => {
        // Password not match
        if (!isMatch) {
          res.statusCode = 400;
          return res.json({ message: "Password not match" });
        }

        // If match, then creating token
        else {
          // Create payload from user data
          const payload = { username: user.username, name: user.name };

          return res.json({
            username: user.username,
            name: user.name,
            token: `Bearer ${tokenUtil.generate(payload)}`
          });
        }
      });
    })
    .catch(err => {
      res.statusCode = 500;
      res.json({ message: "Uppss.. Something wrong " });
    });
});

// @route   POST api/auth/register
// @desc    Register user
// @access  Public
router.post("/register", (req, res) => {
  const name = req.body.name;
  const username = req.body.username;
  const password = req.body.password;

  if (isEmpty(name) || isEmpty(username) || isEmpty(password)) {
    res.statusCode = 400;
    return res.json({
      message: "Name, username, and password field is required"
    });
  } else if (password.length < 6 || password.length > 12) {
    res.statusCode = 400;
    return res.json({
      message: "Password must between 6 and 12 characters"
    });
  }

  const userRef = db.collection("users");

  // Validation username exists
  userRef
    .where("username", "==", username)
    .get()
    .then(snaps => {
      // If username already taken
      if (snaps.size > 0) {
        res.statusCode = 400;
        return res.json({ message: "Username already taken" });
      } else {
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(password, salt, (err, hash) => {
            if (err) throw err;

            userRef
              .add({
                name,
                username,
                password: hash,
                highscore: { value: 0 },
                scores: []
              })
              .then(ref => {
                return res.json({ message: "Successfully register" });
              })
              .catch(err => {
                res.statusCode = 400;
                return res.json({ message: "Failed register" });
              });
          });
        });
      }
    })
    .catch(err => {
      res.statusCode = 500;
      return res.json({ message: "Uppss.. Something wrong " });
    });
});

// @route   GET api/auth/verify
// @desc    Verify user logged with token
// @access  Private
router.get(
  "/verify",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const needToken = req.query.needToken;
    let response = { message: "User has logged in" };

    // Check if verify need new token
    if (!isEmpty(needToken) && needToken === "true") {
      response.token = tokenUtil.generate({
        username: req.user.username,
        name: req.user.name
      });
    }

    res.json(response);
  }
);

// @route   GET api/auth/token
// @desc    Refresh new token
// @access  Private
router.get(
  "/token",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    // Create payload from user data
    const payload = { username: req.user.username, name: req.user.name };

    // Generate and render the token
    res.json({ token: `Bearer ${tokenUtil.generate(payload)}` });
  }
);

module.exports = router;
