const express = require("express");
const passport = require("passport");

const router = express.Router();

const db = require("../../config/firebase");
const isEmpty = require("../../util/isEmpty");

// @route   POST api/game/save-score
// @desc    Add score player to firebase
// @access  Private
router.post(
  "/save-score",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const score = req.body.score;

    if (isEmpty(score)) {
      res.statusCode = 400;
      return res.json({ message: "Score field is required" });
    } else if (!Number.isInteger(parseInt(score))) {
      res.statusCode = 400;
      return res.json({ message: "Score field must a number" });
    }

    // Data to merge with firestore user
    const data = {
      scores: [
        ...req.user.scores,
        {
          timestamp: parseInt(new Date().getTime() / 1000),
          value: parseInt(score)
        }
      ]
    };

    // If this score greater than highscore
    if (score > req.user.highscore.value)
      data.highscore = {
        timestamp: parseInt(new Date().getTime() / 1000),
        value: parseInt(score)
      };

    db.collection("users")
      .doc(req.user.id)
      .set(data, { merge: true })
      .then(result => {
        return res.json({ message: "Successfully add score" });
      })
      .catch(err => {
        console.log(err);
        res.statusCode = 500;
        return res.json({ message: "Uppss.. Something wrong " });
      });
  }
);

// @route   GET api/game/leaderboard
// @desc    Fetch leaderboard from firebase
// @access  Private
router.get(
  "/leaderboard",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const usersRef = db.collection("users");
    usersRef
      .where("highscore.value", ">", 0)
      .orderBy("highscore.value", "desc")
      .get()
      .then(snaps => {
        let data = [];

        snaps.forEach(doc => {
          const temp = doc.data();
          data.push({
            username: temp.username,
            highscore: temp.highscore.value,
            timestamp: temp.highscore.timestamp
          });
        });

        return res.json(data);
      })
      .catch(err => {
        res.statusCode = 500;
        return res.json({ message: "Uppss.. Something wrong " });
      });
  }
);

// @route   GET api/game/stats
// @desc    Get stats from a player
// @access  Private
router.get(
  "/stats",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    let data = {
      username: req.user.username,
      highscore: req.user.highscore,
      scores: req.user.scores,
      average: 0.0
    };

    // Calculate total scores
    if (data.scores.length > 0) {
      let total = 0;
      data.scores.forEach(v => (total += v.value));

      // Average
      data.average = total / data.scores.length;
    }

    return res.json(data);
  }
);

module.exports = router;
