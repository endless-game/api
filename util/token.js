const jwt = require("jsonwebtoken");
const keys = require("../config/keys");

const generate = payload => {
  return jwt.sign(payload, keys.secretKey, { expiresIn: 86400 });
};

module.exports = { generate };
