const current_version = "1.0";

module.exports = require(`../versions/${current_version}`);
