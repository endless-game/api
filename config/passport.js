const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;

const db = require("./firebase");
const keys = require("./keys");

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: keys.secretKey
};

module.exports = passport => {
  passport.use(
    new JwtStrategy(opts, (payload, done) => {
      // Validation user exists with firebase
      db.collection("users")
        .where("username", "==", payload.username)
        .get()
        .then(snapshot => {
          let data = [];
          snapshot.forEach(doc => data.push({ id: doc.id, ...doc.data() }));

          if (data.length < 1) return done(null, false);
          else {
            delete data[0].password;
            return done(null, data[0]);
          }
        })
        .catch(err => {
          console.log(err);
          return done(null, payload);
        });
    })
  );
};
