const firebase = require("firebase");

var config = {
  apiKey: "AIzaSyDogKT8832hprvkMT9DGGqIbKpFJ4raJDo",
  authDomain: "endless-219609.firebaseapp.com",
  databaseURL: "https://endless-219609.firebaseio.com",
  projectId: "endless-219609",
  storageBucket: "endless-219609.appspot.com",
  messagingSenderId: "37779644209"
};

firebase.initializeApp(config);

const settings = { timestampsInSnapshots: true };
const firestore = firebase.firestore();
firestore.settings(settings);

module.exports = firestore;
