module.exports = {
  version: "1.1",
  release: "2018-10-25",
  dependencies: {
    images: [
      {
        file: "http://localhost:3000/public/1.1/images/example.png",
        size: 1000
      }
    ],
    audios: [
      {
        file: "http://localhost:3000/public/1.1/audios/example.mp3",
        size: 2000
      }
    ]
  }
};
