const express = require("express");
const bodyParser = require("body-parser");
const passport = require("passport");

// Define all routers API
const auth = require("./routes/api/auth");
const game = require("./routes/api/game");
const system = require("./routes/api/system");

const app = express();

// Body Parser setup
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Passport middleware
app.use(passport.initialize());
require("./config/passport")(passport);

app.get("/", (req, res) =>
  res.json({
    title: "Endless Game API",
    developer: "Endless Team",
    version: 1.0,
    date: 2018
  })
);

// Serving public assets
app.use("/public", express.static("public"));

// Use routes
app.use("/api/auth", auth);
app.use("/api/game", game);
app.use("/api/system", system);

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`Server running on port ${port}`));
